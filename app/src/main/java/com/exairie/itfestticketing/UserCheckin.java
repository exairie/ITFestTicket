package com.exairie.itfestticketing;

/**
 * Created by exain on 8/27/2017.
 */

public class UserCheckin {
    String name;
    String email;
    long checkin_at;
    String checkin_operator;
    long user_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getCheckin_at() {
        return checkin_at;
    }

    public void setCheckin_at(long checkin_at) {
        this.checkin_at = checkin_at;
    }

    public String getCheckin_operator() {
        return checkin_operator;
    }

    public void setCheckin_operator(String checkin_operator) {
        this.checkin_operator = checkin_operator;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }
}
