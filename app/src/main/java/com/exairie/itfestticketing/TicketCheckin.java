package com.exairie.itfestticketing;

import android.util.Log;
import android.widget.Toast;

/**
 * Created by exain on 8/27/2017.
 */

public class TicketCheckin {
    String key;
    long checkindate;
    long ticketid;
    String registrar;
    String email;
    long register_date;
    private static String TAG = "TICKETCHECKIN";

    public static TicketCheckin complieQR(String qrdata){
        String[] data = qrdata.split(";");
        if (data.length < 4) return null;

        TicketCheckin t = new TicketCheckin();
        try {
            t.setTicketid(Long.parseLong(data[0]));
            t.setEmail(data[1]);
            t.setRegistrar(data[2]);
            t.setRegister_date(Long.parseLong(data[3]));
            return t;
        }catch (Exception e){
            Log.d(TAG, "complieQR: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getCheckindate() {
        return checkindate;
    }

    public void setCheckindate(long checkindate) {
        this.checkindate = checkindate;
    }

    public long getTicketid() {
        return ticketid;
    }

    public void setTicketid(long ticketid) {
        this.ticketid = ticketid;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getRegister_date() {
        return register_date;
    }

    public void setRegister_date(long register_date) {
        this.register_date = register_date;
    }
}
