package com.exairie.itfestticketing;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.exairie.extlib.Configs;
import com.exairie.extlib.ExtActivity;
import com.exairie.extlib.FinishedRequest;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.glxn.qrgen.android.QRCode;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityCheckTicket extends ExtActivity {
    @InjectView(R.id.toolbar2)
    Toolbar toolbar2;
    @InjectView(R.id.qrcode)
    ImageView qrcode;
    @InjectView(R.id.registrar)
    TextView registrar;
    @InjectView(R.id.regdate)
    TextView regdate;
    @InjectView(R.id.email)
    TextView email;
    @InjectView(R.id.phone)
    TextView phone;
    @InjectView(R.id.complete)
    Button complete;
    @InjectView(R.id.institution)
    TextView institution;
    @InjectView(R.id.job)
    TextView job;
    @InjectView(R.id.payment_status)
    TextView paymentStatus;
    private List<TicketRegistration> tickets;

    TicketRegistration ticketRegistration;
    FirebaseDatabase db;
    ProgressDialog dialog;

    boolean offlineSave = false;
    boolean offlinePersistence = false;
    boolean timeout = true;
    long ticketid;
    String qrdata;
    private boolean isSyncSessionInitiated = false;

    public ActivityCheckTicket() {
        super(R.layout.activity_check_ticket);
    }


    private void sync() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Fetching data");
        dialog.setCancelable(false);
        dialog.show();
        Request request = new Request.Builder()
                .get()
                .url(server("api/tickets"))
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                LocalDB db = new LocalDB(thisContext());
                try {
                    JSONObject object = new JSONObject(string);
                    if (object.optString("status").equals("ok")) {
                        TicketRegistration[] registrations = Configs.mapper()
                                .readValue(object.optString("result"), TicketRegistration[].class);


                        for (TicketRegistration registration : registrations) {
                            ContentValues values = new ContentValues();
                            values.put("id", registration.id);
                            values.put("registrar", registration.registrar);
                            values.put("email", registration.email);
                            values.put("register_date", registration.register_date);
                            values.put("institute", registration.institute);
                            values.put("job", registration.job);
                            values.put("ticket_status", registration.ticket_status);
                            values.put("phone", registration.phone);
                            values.put("ticket_validator", registration.ticket_validator);
                            values.put("ticket_validation_date", registration.ticket_validation_date);

                            db.getWritableDatabase().replace("db_registration", "N/A", values);

                            values = null;
                        }
                        runOnUiThread(() -> updateReference());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    db.close();
                }

            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() -> new AlertDialog.Builder(thisContext())
                        .setMessage(msg)
                        .setNegativeButton("close", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show());
            }

            @Override
            public void Finish() {
                dialog.dismiss();
            }
        });
    }

    private void updateReference() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Updating references");
        dialog.setCancelable(false);
        dialog.show();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Container.localTicketRegistrations = null;
                Container.localTicketRegistrations = new ArrayList<>();
                LocalDB db = new LocalDB(thisContext());
                Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM db_registration", new String[]{});

                if (c.getCount() < 1) {
                    if (!isSyncSessionInitiated) {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> {
                                            dialogInterface.dismiss();
                                            sync();
                                        })
                                        .setCancelable(false)
                                        .show());
                        isSyncSessionInitiated = true;
                    } else {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> dialogInterface.dismiss())
                                        .setCancelable(false)
                                        .show());
                    }
                }

                c.moveToFirst();
                while (!c.isAfterLast()) {
                    Container.localTicketRegistrations.add(TicketRegistration.cursor(c));
                    c.moveToNext();
                }
                db.close();
                dialog.dismiss();
                return null;
            }
        };
        task.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tickets = new ArrayList<>();
        db = FirebaseDatabase.getInstance();
        Intent i = getIntent();
        ticketid = i.getLongExtra("id", -1);
        qrdata = i.getStringExtra("qrdata");
        if (ticketid == -1 || qrdata == null) {
            Toast.makeText(thisContext(), "Invalid Parameter", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Fetching data");
        dialog.setCancelable(false);
        dialog.show();

        if (Container.localTicketRegistrations == null) {
            updateReference();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        showData();
    }

    void showData() {
        dialog.dismiss();
        for (TicketRegistration t : Container.ticketRegistrations) {
            if (t.getId() == ticketid) {
                ticketRegistration = t;
                break;
            }
        }
        if (ticketRegistration == null) {
            Toast.makeText(thisContext(), "Ticket Not Found for ID #" + ticketid, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        Bitmap b = QRCode.from(qrdata).bitmap();

        qrcode.setImageBitmap(b);
        registrar.setText(ticketRegistration.getRegistrar());
        institution.setText(ticketRegistration.getInstitute());
        job.setText(ticketRegistration.getJob());
        regdate.setText(DateFormat.format("d-M-yyyy", ticketRegistration.getRegister_date()));
        email.setText(ticketRegistration.getEmail());
        phone.setText(ticketRegistration.getPhone());

        paymentStatus.setText("Unavailable");
        paymentStatus.setTextColor(getResources().getColor(R.color.red_darken_3));

        TicketRegistration localTicket = new TicketRegistration();
        for (TicketRegistration lT : Container.localTicketRegistrations){
            if (lT.getId() == ticketRegistration.getId()){
                ticketRegistration.setRegistrar(lT.getRegistrar());
                ticketRegistration.setInstitute(lT.getInstitute());
                ticketRegistration.setJob(lT.getJob());

                if (lT.getTicket_status().equals("2")){
                    paymentStatus.setText("Paid");
                    paymentStatus.setTextColor(getResources().getColor(R.color.green_darken_3));
                }else if (lT.getTicket_status().equals("1")){
                    paymentStatus.setText("Unconfirmed");
                    paymentStatus.setTextColor(getResources().getColor(R.color.orange_darken_3));
                }
            }
        }

        ConnectivityManager cm = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);

        complete.setOnClickListener(view -> {
            dialog = new ProgressDialog(thisContext());
            dialog.setMessage("Saving data");
            dialog.setCancelable(false);
            dialog.show();
            TicketCheckin checkin = TicketCheckin.complieQR(qrdata);
            DatabaseReference ref = db.getReference("checkins").push();
            checkin.setKey(ref.getKey());
            checkin.setCheckindate(System.currentTimeMillis());


            ChildEventListener lst = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    TicketCheckin chk = dataSnapshot.getValue(TicketCheckin.class);
                    if (checkin.getKey().equals(chk.getKey())){
                        offlinePersistence = true;
                        offlineSave = true;

                    }else{
                        offlinePersistence = false;
                        offlineSave = true;
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            db.getReference("checkins").addChildEventListener(lst);

            ref.setValue(checkin,new DatabaseReference.CompletionListener(){

                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    try{
                        timeout = false;
                        Log.d("OnSaveComplete",String.format("%s",String.valueOf(databaseError != null)));
                        if (!cm.getActiveNetworkInfo().isConnected()) return;
                        dialog.dismiss();
                        if (databaseError != null){
                            new AlertDialog.Builder(thisContext())
                                    .setTitle("Ticket Checkin")
                                    .setMessage("Checkin Warning : " + databaseError.getMessage())
                                    .setNegativeButton("OK", (dialogInterface, i) -> {
                                        dialogInterface.dismiss();
                                        finish();
                                    }).show();
                        }else{
                            new AlertDialog.Builder(thisContext())
                                    .setTitle("Ticket Checkin")
                                    .setMessage("Checkin Successfull")
                                    .setNegativeButton("OK", (dialogInterface, i) -> {
                                        dialogInterface.dismiss();
                                        finish();
                                    }).show();
                        }
                    }catch(Exception exc){
                        exc.printStackTrace();
                    }
                }
            });


            new Handler().postDelayed(() -> {
                if (!timeout) return;
                dialog.dismiss();
                String chkinf = "";
                if (offlineSave){
                    chkinf += "Offline data saved";
                }else{
                    chkinf += "Offline data not saved. Please do manual checkin";
                }

                if (offlinePersistence){
                    chkinf += "\nCheckin persistence guaranteed";
                }else{
                    chkinf += "\nCheckin persistence not guaranteed";
                }
                String finalChkinf = chkinf;
                runOnUiThread(() -> new AlertDialog.Builder(thisContext())
                        .setTitle("Ticket Checkin")
                        .setMessage("Server timeout\n"+ finalChkinf)
                        .setNegativeButton("OK", (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                            finish();
                        }).show());
            },3000);
        });
    }
}
