package com.exairie.itfestticketing;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.exairie.extlib.Configs;
import com.exairie.extlib.ExtActivity;
import com.exairie.extlib.FinishedRequest;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends ExtActivity implements Detector.Processor<Barcode> {
    private static final int REQ_PERMISSION_REARCAMERA = 3;
    FirebaseDatabase db;
    @InjectView(R.id.btn_viewticket)
    Button btnViewticket;
    @InjectView(R.id.toolbar2)
    Toolbar toolbar2;
    @InjectView(R.id.l_username)
    TextView lUsername;
    @InjectView(R.id.recycler_checkins)
    RecyclerView recyclerCheckins;
    @InjectView(R.id.surface)
    SurfaceView cameraView;

    BarcodeDetector detector;
    CameraSource backCamera;

    List<TicketCheckin> checkins;
    Adapter adapter;
    @InjectView(R.id.logout)
    Button logout;
    @InjectView(R.id.email)
    TextView email;
    private boolean ready_detection = false;
    private boolean isSyncSessionInitiated;

    BroadcastReceiver receiver;
    public MainActivity() {
        super(R.layout.activity_main);
        checkins = new ArrayList<>();
        ExtActivity.STATUSBAR_COLOR = R.color.colorPrimaryDark;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQ_PERMISSION_REARCAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        StartCamera();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {

                    }
                } else {
                    Toast.makeText(this, "Please allow application to use backCamera!", Toast.LENGTH_LONG);
                    finish();
                }
            }
            break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ready_detection = true;
        try {
            registerReceiver(receiver,new IntentFilter(FDBService.FDB_CHECKIN_CHANGED));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initCamera();
    }

    @Override
    protected void onDestroy() {
        if (receiver != null)
            unregisterReceiver(receiver);
        super.onDestroy();
    }

    void initCamera() {
        detector = new BarcodeDetector.Builder(thisContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        if (!detector.isOperational()) {
            Log.d("BARCODE", "Barcode Detector Not Operational");
            Toast.makeText(this, "Barcode Detector Not Operational", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        backCamera = new CameraSource.Builder(this, detector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(640, 480)
                .setAutoFocusEnabled(true)
                .build();
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                try {
                    if (ContextCompat.checkSelfPermission(thisContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        String[] requestedPermission = new String[]{Manifest.permission.CAMERA};
                        ActivityCompat.requestPermissions(MainActivity.this, requestedPermission, REQ_PERMISSION_REARCAMERA);
                        return;
                    }

                    StartCamera();
                } catch (IOException e) {
                    Log.e("CAMERA SOURCE ", e.getMessage());
                } catch (SecurityException e) {
                    Log.e("Permission Denied : ", e.getMessage());

                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                StopCamera();
            }
        });

        detector.setProcessor(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (adapter != null)
                    adapter.notifyDataSetChanged();
            }
        };

        startService(new Intent(thisContext(),FDBService.class));

        FirebaseMessaging.getInstance().subscribeToTopic("notifications");
        Log.d("SUBSCRIBE","SUBSCRIBED");
        Container.ticketCheckins = new ArrayList<>();
        Container.ticketRegistrations = new ArrayList<>();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        ;

        adapter = new Adapter();
        recyclerCheckins.setAdapter(adapter);
        recyclerCheckins.setLayoutManager(new LinearLayoutManager(this));

        db = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        try {
            email.setText(user.getEmail());
        } catch (NullPointerException e) {
            e.printStackTrace();
            email.setText("<unknown user>");
        }

        btnViewticket.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityTicketList.class);
            startActivity(i);
        });

        logout.setOnClickListener(view -> {
            FirebaseAuth auth = FirebaseAuth.getInstance();
            auth.signOut();
            Intent i = new Intent(thisContext(), ActivityLogin.class);
            startActivity(i);
            finish();
        });
        db.getReference("tickets").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (Container.ticketRegistrations == null) Container.ticketRegistrations = new ArrayList<TicketRegistration>();
                Container.ticketRegistrations.add(dataSnapshot.getValue(TicketRegistration.class));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        updateReference();
    }
    private void sync() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Fetching data");
        dialog.setCancelable(false);
        dialog.show();
        Request request = new Request.Builder()
                .get()
                .url(server("api/tickets"))
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                LocalDB db = new LocalDB(thisContext());
                try {
                    JSONObject object = new JSONObject(string);
                    if (object.optString("status").equals("ok")) {
                        TicketRegistration[] registrations = Configs.mapper()
                                .readValue(object.optString("result"), TicketRegistration[].class);


                        for (TicketRegistration registration : registrations) {
                            ContentValues values = new ContentValues();
                            values.put("id", registration.id);
                            values.put("registrar", registration.registrar);
                            values.put("email", registration.email);
                            values.put("register_date", registration.register_date);
                            values.put("institute", registration.institute);
                            values.put("job", registration.job);
                            values.put("ticket_status", registration.ticket_status);
                            values.put("phone", registration.phone);
                            values.put("ticket_validator", registration.ticket_validator);
                            values.put("ticket_validation_date", registration.ticket_validation_date);

                            db.getWritableDatabase().replace("db_registration", "N/A", values);

                            values = null;
                        }
                        runOnUiThread(() -> updateReference());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    db.close();
                }

            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() -> new AlertDialog.Builder(thisContext())
                        .setMessage(msg)
                        .setNegativeButton("close", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show());
            }

            @Override
            public void Finish() {
                dialog.dismiss();
            }
        });
    }
    private void updateReference() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Updating references");
        dialog.setCancelable(false);
        dialog.show();

        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Container.localTicketRegistrations = null;
                Container.localTicketRegistrations = new ArrayList<>();
                LocalDB db = new LocalDB(thisContext());
                Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM db_registration", new String[]{});

                if (c.getCount() < 1) {
                    if (!isSyncSessionInitiated) {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> {
                                            dialogInterface.dismiss();
                                            sync();
                                        })
                                        .setCancelable(false)
                                        .show());
                        isSyncSessionInitiated = true;
                    } else {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> dialogInterface.dismiss())
                                        .setCancelable(false)
                                        .show());
                    }
                }

                c.moveToFirst();
                while (!c.isAfterLast()) {
                    Container.localTicketRegistrations.add(TicketRegistration.cursor(c));
                    c.moveToNext();
                }
                db.close();
                dialog.dismiss();
                return null;
            }
        };
        task.execute();
    }
    @Override
    public void release() {

    }

    @Override
    public void receiveDetections(Detector.Detections<Barcode> detections) {
        Log.d("DETECTIONS", "RECEIVE");
        final SparseArray<Barcode> detected = detections.getDetectedItems();
        if (detected.size() > 0 && ready_detection) {
            Log.d("DETECTIONS", "DETECTED");
            String value = detected.valueAt(0).displayValue;
            try {
                ready_detection = false;
                TicketCheckin checkin = TicketCheckin.complieQR(value);
                if (checkin != null) {
                    Intent i = new Intent(thisContext(), ActivityCheckTicket.class);
                    i.putExtra("id", checkin.getTicketid());
                    i.putExtra("qrdata", value);
                    startActivity(i);
                } else {
                    throw new Exception();
                }

            } catch (Exception exc) {
                exc.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ready_detection = false;
                        new AlertDialog.Builder(thisContext())
                                .setTitle("Barcode error")
                                .setMessage("Invalid barcode. Please scan a correct one!")
                                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        ready_detection = true;
                                    }
                                }).show();
                    }
                });
            } finally {

            }
        }
    }

    void StopCamera() {
        backCamera.stop();
    }

    void StartCamera() throws SecurityException, IOException {
        backCamera.start(cameraView.getHolder());

    }

    class Adapter extends RecyclerView.Adapter<Adapter.Holder> {


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_checkin, parent, false);

            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            TicketCheckin c = Container.ticketCheckins.get(position);
            holder.name.setText(c.getRegistrar());
            holder.email.setText(c.getEmail());
            holder.checkindate.setText(DateFormat.format("d-M-yyyy HH:mm:ss", c.getCheckindate()));
        }

        @Override
        public int getItemCount() {
            return Container.ticketCheckins.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.name)
            TextView name;
            @InjectView(R.id.email)
            TextView email;
            @InjectView(R.id.checkindate)
            TextView checkindate;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }
    }
}
