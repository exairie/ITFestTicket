package com.exairie.itfestticketing;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exairie.extlib.Configs;
import com.exairie.extlib.ExtActivity;
import com.exairie.extlib.FinishedRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import net.glxn.qrgen.android.QRCode;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityTicketList extends ExtActivity {
    FirebaseDatabase db;
    List<TicketRegistration> tickets;
    @InjectView(R.id.toolbar2)
    Toolbar toolbar2;

    @InjectView(R.id.t_filter)
    EditText tFilter;
    @InjectView(R.id.relativeLayout)
    LinearLayout relativeLayout;
    @InjectView(R.id.recycler)
    RecyclerView recycler;


    Adapter adapter;
    @InjectView(R.id.numoftickets)
    TextView numoftickets;
    @InjectView(R.id.btn_sync)
    Button btnSync;
    @InjectView(R.id.l_paid)
    TextView lPaid;
    @InjectView(R.id.l_unconfirmed)
    TextView lUnconfirmed;
    @InjectView(R.id.l_unavailable)
    TextView lUnavailable;
    private boolean isSyncSessionInitiated = false;

    public ActivityTicketList() {
        super(R.layout.activity_ticket_list);
        tickets = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new Adapter();
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(thisContext()));

        db = FirebaseDatabase.getInstance();

        db.getReference("tickets").orderByChild("register_date").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                TicketRegistration registration = dataSnapshot.getValue(TicketRegistration.class);
                for (int i = 0; i < tickets.size(); i++) {
                    if (tickets.get(i).getId() == registration.getId()) {
                        return;
                    }
                }
                tickets.add(registration);
                updateCounter();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                TicketRegistration registration = dataSnapshot.getValue(TicketRegistration.class);
                for (int i = 0; i < tickets.size(); i++) {
                    if (tickets.get(i).getId() == registration.getId()) {
                        tickets.get(i).setEmail(registration.getEmail());
                        tickets.get(i).setInstitute(registration.getInstitute());
                        tickets.get(i).setJob(registration.getJob());
                        tickets.get(i).setRegister_date(registration.getRegister_date());
                        tickets.get(i).setRegistrar(registration.getRegistrar());

                        adapter.notifyDataSetChanged();
                        return;
                    }
                }

                tickets.add(dataSnapshot.getValue(TicketRegistration.class));
                updateCounter();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                TicketRegistration registration = dataSnapshot.getValue(TicketRegistration.class);
                for (int i = 0; i < tickets.size(); i++) {
                    if (tickets.get(i).getId() == registration.getId()) {
                        tickets.remove(tickets.get(i));
                        updateCounter();
                        adapter.notifyDataSetChanged();
                        return;
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        tFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.setTextFilter(editable.toString());
            }
        });

        btnSync.setOnClickListener(view -> sync());

        if (Container.localTicketRegistrations == null) {
            updateReference();
        }else{
            updateSummary();
        }
    }

    private void updateSummary() {
        int paid = 0;
        int uncon = 0;
        int unavail = 0;
        for (TicketRegistration t : Container.localTicketRegistrations){
            Log.d("STAT",String.format("%s : %s -> %d",t.getTicket_status(),"TEST",-99));
            if (t.getTicket_status().equals("2")){
                paid++;
                Log.d("STAT",String.format("%s : %s -> %d",t.getTicket_status(),"PAID",paid));
            }else if (t.getTicket_status().equals("1")){
                uncon++;
                Log.d("STAT",String.format("%s : %s -> %d",t.getTicket_status(),"UNCON",uncon));
            }else{
                unavail++;
                Log.d("STAT",String.format("%s : %s -> %d",t.getTicket_status(),"UNAV",unavail));
            }
        }

        lPaid.setText(String.valueOf(paid));
        lUnavailable.setText(String.valueOf(unavail));
        lUnconfirmed.setText(String.valueOf(uncon));
    }

    private void sync() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Fetching data");
        dialog.setCancelable(false);
        dialog.show();
        Request request = new Request.Builder()
                .get()
                .url(server("api/tickets"))
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                LocalDB db = new LocalDB(thisContext());
                try {
                    JSONObject object = new JSONObject(string);
                    if (object.optString("status").equals("ok")) {
                        TicketRegistration[] registrations = Configs.mapper()
                                .readValue(object.optString("result"), TicketRegistration[].class);


                        for (TicketRegistration registration : registrations) {
                            ContentValues values = new ContentValues();
                            values.put("id", registration.id);
                            values.put("registrar", registration.registrar);
                            values.put("email", registration.email);
                            values.put("register_date", registration.register_date);
                            values.put("institute", registration.institute);
                            values.put("job", registration.job);
                            values.put("ticket_status", registration.ticket_status);
                            values.put("phone", registration.phone);
                            values.put("ticket_validator", registration.ticket_validator);
                            values.put("ticket_validation_date", registration.ticket_validation_date);

                            db.getWritableDatabase().replace("db_registration", "N/A", values);

                            values = null;
                        }

                        runOnUiThread(() -> updateReference());

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    db.close();
                }

            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() -> new AlertDialog.Builder(thisContext())
                        .setMessage(msg)
                        .setNegativeButton("close", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show());
            }

            @Override
            public void Finish() {
                dialog.dismiss();
            }
        });
    }

    private void updateReference() {
        ProgressDialog dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Updating references");
        dialog.setCancelable(false);
        dialog.show();

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Container.localTicketRegistrations = null;
                Container.localTicketRegistrations = new ArrayList<>();
                LocalDB db = new LocalDB(thisContext());
                Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM db_registration", new String[]{});

                if (c.getCount() < 1) {
                    if (!isSyncSessionInitiated) {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> {
                                            dialogInterface.dismiss();
                                            sync();
                                        })
                                        .setCancelable(false)
                                        .show());
                        isSyncSessionInitiated = true;
                    } else {
                        runOnUiThread(() ->
                                new AlertDialog.Builder(thisContext())
                                        .setMessage("Server Sync not available.")
                                        .setPositiveButton("Sync", (dialogInterface, i) -> dialogInterface.dismiss())
                                        .setCancelable(false)
                                        .show());
                    }
                }

                c.moveToFirst();
                while (!c.isAfterLast()) {
                    Container.localTicketRegistrations.add(TicketRegistration.cursor(c));
                    c.moveToNext();
                }
                db.close();
                dialog.dismiss();

                runOnUiThread(() -> updateSummary());
                return null;
            }
        };
        task.execute();
    }

    private void updateCounter() {
        try {
            numoftickets.setText(String.format("Total : %d", tickets.size()));
        } catch (Exception exc) {

        } finally {

        }
    }

    class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
        boolean useFilter = false;
        List<TicketRegistration> _filteredList;


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_ticketlist, parent, false);

            return new Holder(v);
        }

        public void setTextFilter(String filter) {
            if (filter.length() < 1) {
                useFilter = false;
                notifyDataSetChanged();
            } else {
                _filteredList = new ArrayList<>();
                for (TicketRegistration t : tickets) {
                    if (t.getRegistrar().toLowerCase().contains(filter.toLowerCase())
                            || t.getInstitute().toLowerCase().contains(filter.toLowerCase())
                            || t.getEmail().toLowerCase().contains(filter.toLowerCase())) {
                        _filteredList.add(t);
                    }
                }
                useFilter = true;
                notifyDataSetChanged();
            }
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            TicketRegistration t;
            if (useFilter) {
                t = _filteredList.get(position);

            } else {
                t = tickets.get(position);
            }

            String payment;
            int _payment = 0;

            holder.imgStat.setImageResource(R.drawable.ic_warning_red_700_24dp);
            payment = "Unpaid";

            for (TicketRegistration localTicketReg : Container.localTicketRegistrations) {
                if (localTicketReg.id == t.getId()) {
                    if (localTicketReg.getTicket_status().equals("2")) {
                        holder.imgStat.setImageResource(R.drawable.ic_check_circle_green_700_24dp);
                        payment = "Payment Complete";
                    } else if (localTicketReg.getTicket_status().equals("1")) {
                        holder.imgStat.setImageResource(R.drawable.ic_info_yellow_700_24dp);
                        payment = "Payment Unconfirmed";
                    }
                }
            }

            String qrdata = String.format("%s;%s;%s;%s",
                    t.getId(), t.getEmail(), t.getRegistrar(), t.getRegister_date());
            Bitmap qr = null;
            qr = QRCode.from(qrdata)
                    .bitmap();
            holder.qrcode.setImageBitmap(qr);

            holder.registrar.setText(t.getRegistrar());
            holder.institution.setText(String.format("%s/%s", t.getInstitute(), t.getJob()));
            holder.regdate.setText(DateFormat.format("d-M-yyyy HH:mm:ss", t.getRegister_date()));
            holder.email.setText(String.format("%s\n%s", t.getEmail(), payment));
        }

        @Override
        public int getItemCount() {
            return useFilter ? (_filteredList == null ? 0 : _filteredList.size()) : (tickets == null ? 0 : tickets.size());
        }

        class Holder extends RecyclerView.ViewHolder {
            public int checkedin = -1;
            @InjectView(R.id.qrcode)
            ImageView qrcode;
            @InjectView(R.id.registrar)
            TextView registrar;
            @InjectView(R.id.institution)
            TextView institution;
            @InjectView(R.id.regdate)
            TextView regdate;
            @InjectView(R.id.img_stat)
            ImageView imgStat;
            @InjectView(R.id.email)
            TextView email;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }
    }

}
