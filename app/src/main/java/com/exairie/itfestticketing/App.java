package com.exairie.itfestticketing;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by exain on 8/27/2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase.getInstance().getReference("tickets").keepSynced(true);
    }
}
