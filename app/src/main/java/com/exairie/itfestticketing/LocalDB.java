package com.exairie.itfestticketing;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by exain on 10/23/2017.
 */

public class LocalDB extends SQLiteOpenHelper {

    private static final String name = "ldb.dat";

    public LocalDB(Context context) {
        super(context, name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase d) {
        d.execSQL("CREATE TABLE IF NOT EXISTS db_registration(" +
                "id integer primary key," +
                "registrar varchar(255)," +
                "email varchar(255)," +
                "register_date integer," +
                "institute varchar(255)," +
                "job varchar(255)," +
                "ticket_status varchar(255)," +
                "phone varchar(255)," +
                "ticket_validator integer," +
                "ticket_validation_date integer" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
