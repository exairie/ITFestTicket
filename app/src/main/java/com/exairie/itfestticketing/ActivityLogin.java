package com.exairie.itfestticketing;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.exairie.extlib.ExtActivity;
import com.exairie.extlib.FinishedRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

import butterknife.InjectView;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityLogin extends ExtActivity {
    private static final String TAG = "FirebaseLogin";
    @InjectView(R.id.toolbar2)
    Toolbar toolbar2;
    @InjectView(R.id.t_email)
    TextInputEditText tEmail;
    @InjectView(R.id.t_password)
    TextInputEditText tPassword;
    @InjectView(R.id.btn_login)
    Button btnLogin;
    @InjectView(R.id.btn_register)
    Button btnRegister;
    private ProgressDialog dialog;

    public ActivityLogin() {
        super(R.layout.activity_login);
    }

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            Intent i = new Intent(thisContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Logging In");
        dialog.setCancelable(false);
        dialog.show();
        auth.signInWithEmailAndPassword(tEmail.getText().toString(),tPassword.getText().toString()).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()){
                Log.d(TAG, "signInWithEmail: success");
                FirebaseUser user = auth.getCurrentUser();
                if (user != null) {
                    Intent i = new Intent(thisContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }else{
                Log.w(TAG, "signInWithEmail:failure", task.getException());
                Toast.makeText(thisContext(), "Authentication failed. " + task.getException().getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        });
    }

    @OnClick(R.id.btn_register)
    public void onBtnRegisterClicked() {
        dialog = new ProgressDialog(thisContext());
        dialog.setMessage("Registering");
        dialog.setCancelable(false);
        dialog.show();
        FormBody body = new FormBody.Builder()
                .add("email",tEmail.getText().toString())
                .build();
        Request request = new Request.Builder()
                .url(server("api/checkuser"))
                .post(body)
                .build();
        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                auth.createUserWithEmailAndPassword(tEmail.getText().toString(),tPassword.getText().toString()).addOnCompleteListener(ActivityLogin.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Log.d(TAG, "signInWithEmail: success");
                            FirebaseUser user = auth.getCurrentUser();
                            if (user != null) {
                                Intent i = new Intent(thisContext(), MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }else{
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                onBtnLoginClicked();
                                return;
                            }
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(thisContext(), "Authentication failed." + task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() -> {
                    new AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",(dialogInterface, i) -> dialogInterface.dismiss())
                            .show();
                });
            }

            @Override
            public void Finish() {
                dialog.dismiss();
            }
        });

    }
}
