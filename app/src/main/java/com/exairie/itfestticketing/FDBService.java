package com.exairie.itfestticketing;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import com.exairie.extlib.Configs;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;

public class FDBService extends Service {
    public static final String FDB_CHECKIN_CHANGED = "com.exairie.itfestticketing.FDB_CHANGE_CHECKIN";

    public FDBService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        db.getReference("checkins").orderByChild("checkindate").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (Container.ticketCheckins == null) Container.ticketCheckins = new ArrayList<TicketCheckin>();
                TicketCheckin checkin = dataSnapshot.getValue(TicketCheckin.class);
                checkin.setKey(dataSnapshot.getKey());
                int i;
                for (i = 0; i < Container.ticketCheckins.size(); i++) {
                    if (checkin.getKey().equals(Container.ticketCheckins.get(i).getKey())){
                        return;
                    }
                }
                for (i = 0; i < Container.ticketCheckins.size(); i++) {
                    if (Container.ticketCheckins.get(i).getCheckindate() < checkin.getCheckindate())
                        i++;
                }

                if (i == Container.ticketCheckins.size() - 1) {
                    Container.ticketCheckins.add(checkin);
                } else {
                    Container.ticketCheckins.add(i - 1 == -1 ? 0 : i - 1, checkin);
                }
                sendBroadcast(new Intent(FDB_CHECKIN_CHANGED));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                TicketCheckin ndata = dataSnapshot.getValue(TicketCheckin.class);
                ndata.setKey(dataSnapshot.getKey());
                for (int i = 0; i < Container.ticketCheckins.size(); i++) {
                    if (Container.ticketCheckins.get(i).getKey().equals(dataSnapshot.getKey())) {
                        Container.ticketCheckins.get(i).setRegistrar(ndata.getRegistrar());
                        Container.ticketCheckins.get(i).setRegister_date(ndata.getRegister_date());
                        Container.ticketCheckins.get(i).setEmail(ndata.getEmail());
                        Container.ticketCheckins.get(i).setCheckindate(ndata.getCheckindate());
                        Container.ticketCheckins.get(i).setTicketid(ndata.getTicketid());
                        break;
                    }
                }
                sendBroadcast(new Intent(FDB_CHECKIN_CHANGED));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                TicketCheckin ndata = dataSnapshot.getValue(TicketCheckin.class);
                try {
                    Log.d("REM",String.format("remove %s",Configs.mapper().writeValueAsString(ndata)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ndata.setKey(dataSnapshot.getKey());
                for (int i = 0; i < Container.ticketCheckins.size(); i++) {
                    try {
                        Log.d("REMTRY",String.format("remove %s",Configs.mapper().writeValueAsString(Container.ticketCheckins.get(i))));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (Container.ticketCheckins.get(i).getKey().equals(ndata.getKey())) {
                        Log.d("MATCH",String.format("remove %s == %s",Container.ticketCheckins.get(i).getKey(),ndata.getKey()));
                        Log.d("DELETE",String.valueOf(Container.ticketCheckins.remove(i)));
                        break;
                    }
                }
                sendBroadcast(new Intent(FDB_CHECKIN_CHANGED));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
