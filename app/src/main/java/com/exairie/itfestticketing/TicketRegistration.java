package com.exairie.itfestticketing;

import android.database.Cursor;

/**
 * Created by exain on 8/27/2017.
 */

public class TicketRegistration {
    public long id;
    public String registrar;
    public String email;
    public long register_date;
    public String institute;
    public String job;
    public String ticket_status;
    public String phone;
    public long ticket_validator;
    public long ticket_validation_date;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getRegister_date() {
        return register_date;
    }

    public void setRegister_date(long register_date) {
        this.register_date = register_date;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getTicket_status() {
        return ticket_status;
    }

    public void setTicket_status(String ticket_status) {
        this.ticket_status = ticket_status;
    }

    public long getTicket_validator() {
        return ticket_validator;
    }

    public void setTicket_validator(long ticket_validator) {
        this.ticket_validator = ticket_validator;
    }

    public long getTicket_validation_date() {
        return ticket_validation_date;
    }

    public void setTicket_validation_date(long ticket_validation_date) {
        this.ticket_validation_date = ticket_validation_date;
    }

    public static TicketRegistration cursor(Cursor c){
        TicketRegistration o = new TicketRegistration();
        o.id = c.getLong(c.getColumnIndexOrThrow("id"));
        o.registrar = c.getString(c.getColumnIndexOrThrow("registrar"));
        o.email = c.getString(c.getColumnIndexOrThrow("email"));
        o.register_date = c.getLong(c.getColumnIndexOrThrow("register_date"));
        o.institute = c.getString(c.getColumnIndexOrThrow("institute"));
        o.job = c.getString(c.getColumnIndexOrThrow("job"));
        o.ticket_status = c.getString(c.getColumnIndexOrThrow("ticket_status"));
        o.phone = c.getString(c.getColumnIndexOrThrow("phone"));
        o.ticket_validator = c.getInt(c.getColumnIndexOrThrow("ticket_validator"));
        o.ticket_validation_date = c.getInt(c.getColumnIndexOrThrow("ticket_validation_date"));

        return o;
    }
}
