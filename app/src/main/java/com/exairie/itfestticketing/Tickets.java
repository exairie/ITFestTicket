package com.exairie.itfestticketing;

/**
 * Created by exain on 8/27/2017.
 */

public class Tickets {
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    long id;
    String registrar;
    String email;
    long register_date;
    String qr_string;

    public String getRegistrar() {
        return registrar;
    }

    public void setRegistrar(String registrar) {
        this.registrar = registrar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getRegister_date() {
        return register_date;
    }

    public void setRegister_date(long register_date) {
        this.register_date = register_date;
    }

    public String getQr_string() {
        return qr_string;
    }

    public void setQr_string(String qr_string) {
        this.qr_string = qr_string;
    }
}
