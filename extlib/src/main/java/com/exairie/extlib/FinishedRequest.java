package com.exairie.extlib;

import okhttp3.Response;

/**
 * Created by exain on 8/25/2017.
 */

public interface FinishedRequest{
    @Deprecated
    void OnSuccess(Response response);
    void OnSuccess(String string);
    void OnFail(String msg);
    void Finish();
}

