package com.exairie.extlib;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 7/17/2017.
 */

public class RequestResult {
    @JsonProperty("status")
    public String status;
    @JsonProperty("msg")
    public String message;
    @JsonProperty("code")
    int code;
}
